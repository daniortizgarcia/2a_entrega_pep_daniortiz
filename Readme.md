## Ejercicio Final 2ª Evaluación

- Utilizada la metodología BEM.

- Utilización de flexbox en la aplicación.
  - Utilizado en el li del nav del header (.nav__li).
  - Utilizado en el content del inicio (.containerContent).
  - Utilizado en el container de las tarifas (.containerTariffs).
  - Utilizado en el header de las tarifas (.headerSection).
  - Utilizado en el body de subtarifas y en la sección de las subtarifas (.bodySubTariffs, .bodySubTariffs__section).
  - Utilizado en articulo y la sección de las familias del catálogo (.family__article, .family__section).
  - Utilizado en el content del contacto (.contactContent).
  - Utilizado en la sección del footer (.footerSection).
  - Utilizado en el content cookies, nosotros (.articleMain).

- Utilización de Grid en la aplicación.
  - Utilizado en el ul del nav del header (.nav__ul).
  - Utilizado en el filtrado de artículos del catálogo (.filter__article).
  - Utilizado en el artículo del catálogo (.catalogue__article).
  - Utilizado en el footer (.footer).

- Utilizado CSS3
  - Selectores de atributo en el inicio al final.

  - Pseudo-clase de vinculo en el inicio al final.

  - Utilización de box-sizing.
    - Utilizado en el formulario de contacto (.contactForm).
    - Utilizado en el input del formulario de contacto (.contactForm__input).
    - Utilizado en el textarea del formulario de contacto (.contactForm__textarea).

  - Utilizacion de box-shadow
    - Utilizado en el sección de tarifas (.tariffsSection).

  - Utilización de varias tipografías.
    - text-shadow 
    - @font-face
    - text-transform
  
  - Utilización de background (.map).
    - background-image
    - background-size
    - background-repeat
    - background-clip

  - Utilización de transform.
    - Utilizado rotateZ en la primera tarifa del inicio (.tariff1).
    - Utilizado skewX en la tercera tarifa del inicio (.tariff3).
    - Utilizado rotate en la animación slide del carrusel (@keyframes slide).

  - Utilización de transitions.
    - Utilizado en prev y next del carrusel en el inicio (.jumbotron__prev, .jumbotron__next).
    - Utilizado en los puntos del carrusel en el inicio (.dot).
    - Utilizado en la sección del cuerpo de las tarifas (.bodySection__section).
 
  - Utilización de animation
    - Utilizado en el SVG del carrusel en el inicio (slide__image, @keyframes slide)
    - Utilizado en la segunda tarifa del inicio (.tariff2, @keyframes tariff)
  
  - Utilización de clip-path
    - Utilizado inset en el cookies primer imagen (.cookiesImage1)

  - Utilización de mask
    - Utilizado en el cookies primer imagen (.cookiesImage2)

- Imágenes y la Web
  - Utilizado un icono con formato .ico
  - Utilizado srcset en el modulo de Nosotros.
  - Utilizado SVGs  en el header el icono de Wifibytes, en el carrusel con mi nombre.
  - Utilizadas imágenes con el formato JPEG para mayor optimización y optimizadas con la pagina compress.io.
  - Test con lighthouse del index.html en el modulo de nosotros.

- Responsive
  - Utilizado 5 media querys.
  - Todas las páginas se ven bien hasta el tamaño mínimo 320px.

- Utilizado scss en la aplicación.
  - style.scss.
  - _footer.scss.

- Accesibilidad
  - Utilizada extensión del navegador Wave con casi ningún Error(Saltan errores al hacer el menú), nigún Warning.
  - Utilizado aria-label en los links para darle mas accesibilidad a la web.

- Utilización de font awesome para los iconos.

- Utilización de iconos
  - Icono .ico
  - Icono de 32x32 y 16x16 .png
  - Icono de Android Crhome 192x192
  - Icono de Apple Touch

- Utilización de @media print
  - Página de tarifas adaptada para imprimir

- Mostrar y ocultar menu con un checkbox con la propiedad checked en todos los nav (#menu-1:checked ~ #vertical__nav)
